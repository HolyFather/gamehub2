using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private PlayerInputActions playerInputActions;
    private Rigidbody PlayerRigidbody;
    private Vector3 startPosition;

    [SerializeField]
    private float MoveSpeed = 600f;

    private void Awake()
    {
        playerInputActions = new PlayerInputActions();
        PlayerRigidbody = GetComponent<Rigidbody>();
        startPosition = transform.position;

        playerInputActions.Car.ResetPosition.performed += context => ResetPosition();
    }

    private void OnEnable()
    {
        playerInputActions.Enable();
    }

    private void OnDisable()
    {
        playerInputActions.Disable();
    }

    private void FixedUpdate()
    {
        Vector2 MoveDirection = playerInputActions.Car.Move.ReadValue<Vector2>();
        Move(MoveDirection);
        if (Input.GetKey(KeyCode.Mouse1))
        {
            MoveSpeed = 1000f;
        }    
        else
        {
            MoveSpeed = 600f;
        }
    }

    private void Move(Vector2 direction)
    {
        PlayerRigidbody.velocity = new Vector3(direction.x * MoveSpeed * Time.fixedDeltaTime, 0f, direction.y * MoveSpeed * Time.fixedDeltaTime);
    }

    private void ResetPosition()
    {
        PlayerRigidbody.MovePosition(startPosition);
        PlayerRigidbody.MoveRotation(Quaternion.identity);
    }
}
